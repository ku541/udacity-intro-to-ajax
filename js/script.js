
function loadData() {

    var $body = $('body');
    var $wikiElem = $('#wikipedia-links');
    var $nytHeaderElem = $('#nytimes-header');
    var $nytElem = $('#nytimes-articles');
    var $greeting = $('#greeting');

    // clear out old data before new request
    $wikiElem.text("");
    $nytElem.text("");

    var streetString = $("#street").val();
    var cityString = $("#city").val();
    var address = streetString + ', ' + cityString;
    $greeting.text('So, you want to live at ' + address + '?');

    var streetViewURL = 'https://maps.googleapis.com/maps/api/streetview?size=1366x768&location=' + address;
    $body.append('<img class="bgimg" src="' + streetViewURL + '">');

    var nyTimesURL = 'http://api.nytimes.com/svc/search/v2/articlesearch.json?q=' + cityString + '&sort=newest&api-key=591fc063e1e73f7b3160e2dd96aee7c2:2:74310935';
    $.getJSON(nyTimesURL, function(data) {
      $nytHeaderElem.text('New York Times Articles About ' + cityString);
      articles = data.response.docs;
      for (var i = 0; i < articles.length; i++) {
        var article = articles[i];
        $nytElem.append('<li class="article">' + '<a href="' + article.web_url + '">' + article.headline.main + '</a>' + '<p>' + article.snippet + '</p>' + '</li>');
      };
    }).error(function(e) {
      $nytHeaderElem.text('New York Times Articles Could Not Be Loaded.');
      console.log(e);
    });

    var wikiURL = 'https://en.wikipedia.org/w/api.php?action=opensearch&search=' + cityString + '&format=json&callback=wikiCallback';

    var wikiRequestTimeout = setTimeout(function() {
      $wikiElem.text('Failed To Get Wikipedia Resources.');
    }, 8000);

    $.ajax({
      url: wikiURL,
      dataType: "jsonp",
      success: function(response) {
        var articleList = response[1];

        for (var i = 0; i < articleList.length; i++) {
          articleStr = articleList[i];
          var url = 'https://en.wikipedia.org/wiki/' + articleStr;
          $wikiElem.append('<li><a href="' + url + '">' + articleStr + '</a></li>');
        };

        clearTimeout(wikiRequestTimeout);
      }
    });

    return false;
};

$('#form-container').submit(loadData);
